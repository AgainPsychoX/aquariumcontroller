#pragma once

void communicationHandler(BluetoothQwertyServer& serial)
{
	switch (serial.read()) {
    case 'd':
			switch (serial.read()) {
				case '=': {
					// Set date command
					PCF8583_controller.year   = serial.read() + 2000;
					PCF8583_controller.month  = serial.read();
					PCF8583_controller.day    = serial.read();
					PCF8583_controller.set_time();
					return;
				};
				
				case '?': {
					// Get date command
					serial.write('d');
					serial.write('=');
					serial.write(PCF8583_controller.year - 2000);
					serial.write(PCF8583_controller.month);
					serial.write(PCF8583_controller.day);
					return;
				};

				default:
					break;
			}
			break;
		
		case 't': {
			const byte next = serial.read();
      // Time related commands
      switch (next) {
        case '=': {
          // Set time command
          PCF8583_controller.hour   = serial.read();
          PCF8583_controller.minute = serial.read();
          PCF8583_controller.second = serial.read();
          PCF8583_controller.set_time();
          return;
        };
        
        case '?': {
          // Get time command
          serial.write('t');
          serial.write('=');
          serial.write(PCF8583_controller.hour);
          serial.write(PCF8583_controller.minute);
          serial.write(PCF8583_controller.second);
          return;
        };
        
        default:
          break;
      }
		} break;
			
		case 'p': {
      if (serial.peek() == 'i') {
        // Ping-Pong
        serial.write('p');
        serial.write('o');
        serial.write('n');
        serial.write('g');
        return;
      }
      
      byte pwmID = serial.read();
			if (pwmID < PWMPins_Length) {
				switch (serial.read()) {
					case '=': {
						// Set PWM controllers settings
						TimedPWNControllers[pwmID].settings.maxPower        = serial.read();
						TimedPWNControllers[pwmID].settings.onTime.hour     = serial.read();
						TimedPWNControllers[pwmID].settings.onTime.minute   = serial.read();
						TimedPWNControllers[pwmID].settings.offTime.hour    = serial.read();
						TimedPWNControllers[pwmID].settings.offTime.minute  = serial.read();
						TimedPWNControllers[pwmID].settings.risingUpTime    = serial.read();
						TimedPWNControllers[pwmID].update(Time_hhmmss{(byte)PCF8583_controller.hour, (byte)PCF8583_controller.minute, (byte)PCF8583_controller.second});
						TimedPWNControllers[pwmID].saveSettings(PWMSettingsEEPROMAddress + sizeof(TimedPWMController::Settings) * pwmID);
						return;
					};
					
					case '?': {
						// Get time command
						serial.write('p');
						serial.write(pwmID);
						serial.write('=');
						serial.write(TimedPWNControllers[pwmID].settings.maxPower);
						serial.write(TimedPWNControllers[pwmID].settings.onTime.hour);
						serial.write(TimedPWNControllers[pwmID].settings.onTime.minute);
						serial.write(TimedPWNControllers[pwmID].settings.offTime.hour);
						serial.write(TimedPWNControllers[pwmID].settings.offTime.minute);
						serial.write(TimedPWNControllers[pwmID].settings.risingUpTime);
						return;
					};
					
					case '%': {
						// Get current power
						serial.write('p');
						serial.write(pwmID);
						serial.write('%');
						serial.write(TimedPWNControllers[pwmID].currentPower);
						return;
					};

					default:
						break;
				}
			}
		} break;
    
    case 's': {
      serial.write('s');
      serial.write('%');
      
      serial.write('p');
      for (byte i = 0; i < PWMPins_Length; i++) {
        serial.write(TimedPWNControllers[i].currentPower);
      }
      
      serial.write('t');
      for (byte i = 0; i < DS18B20_sensors_num; i++) {
        serial.write(static_cast<byte>(static_cast<int>(DS18B20_value[i])));
        serial.write(static_cast<byte>(static_cast<int>((DS18B20_value[i] - static_cast<int>(DS18B20_value[i])) * 100)));
      }
      
      serial.write('w');
      serial.write(static_cast<byte>(static_cast<int>(phSensors[0].value)));
      serial.write(static_cast<byte>(static_cast<int>((phSensors[0].value - static_cast<int>(phSensors[0].value)) * 100)));
    } break;
    
		default:
			break;
	}

	serial.write('?');
}
