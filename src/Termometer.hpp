#pragma once

////////////////////////////////////////////////////////////////////////////////
// Heading

// Library for DS18B20 termometer
#include <DS18B20.h>

// OneWire protocol
OneWire oneWire(9); // Pin D9; na razie tylko do termometrow

// Number of DS18B20
#define DS18B20_sensors_num 2

// Adresses of DS18B20's
const byte DS18B20_address[DS18B20_sensors_num][8] PROGMEM = {
  0x28, 0x79, 0x84, 0x48, 0x4, 0x0, 0x0, 0xA6,
  0x28, 0xEF, 0x19, 0x97, 0xA, 0x0, 0x0, 0x91
};

// DS18B20 controllers
DS18B20 DS18B20_controller(&oneWire);

// Buffer for values
float DS18B20_value[DS18B20_sensors_num];

// Buzzer pin (alarm)
#define Buzzer_PIN 2

#define ALARM_TIME 10000

// Alarm info structures
struct SimpleAlarm
{
  /* Fields */
  unsigned long lastTimestamp;
  unsigned long passedTime = 0;
  const uint16_t ringTime;
  
  // Settings 
  struct Settings {
    bool active = true;
  } settings;
  
  /* Operators */
  SimpleAlarm(const uint16_t ringTime)
    : ringTime(ringTime)
  {}
  
  /* Methods */
  void update(bool condit)
  {
    if (passedTime > 0) {
      const unsigned long timeDiff = millis() - lastTimestamp;
      lastTimestamp = millis();
      
      passedTime += timeDiff;
      
      if (passedTime > ALARM_TIME) {
        passedTime = 0;
        
        // OFF
        digitalWrite(Buzzer_PIN, LOW);
      }
      else if (settings.active) {
        // Update
        if (millis() % (ringTime << 1) > ringTime) {
          digitalWrite(Buzzer_PIN, HIGH);
        }
        else {
          digitalWrite(Buzzer_PIN, LOW);
        }
      }
    }
    else if (condit) {
      // ON
      passedTime = 1;
    }
  }
};

SimpleAlarm temperatureAlarms[2] = {
  {1500},
  {300}
};



////////////////////////////////////////////////////////////////////////////////
// Update

inline void updateTermometer()
{
  // Update values buffer
  if (DS18B20_controller.available()) {
    for (byte i = 0; i < DS18B20_sensors_num; i++) {
      DS18B20_value[i] = DS18B20_controller.readTemperature(FA(DS18B20_address[i]));
    }
    DS18B20_controller.request();
  }
  
  // Update temperature alarms
  temperatureAlarms[0].update(25 > DS18B20_value[0] || DS18B20_value[0] > 30);
  temperatureAlarms[1].update(25 > DS18B20_value[1] || DS18B20_value[1] > 30);
}



////////////////////////////////////////////////////////////////////////////////
// Setup

inline void setupTermometer()
{
  // Init controller
  DS18B20_controller.begin();
  DS18B20_controller.request();
  
  // Alarm
  pinMode(Buzzer_PIN, OUTPUT);
  digitalWrite(Buzzer_PIN, LOW);
  
  // First update
  updateTermometer();
}


