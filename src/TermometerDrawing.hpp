#pragma once

////////////////////////////////////////////////////////////////////////////////
// Drawing

// @TODO ? bufforowanie char* zamiast float?
void printTemperature(float temperature)
{
  lcd.print(String(temperature, 1));
  lcd.write(0b11011111);
  lcd.write('C');
}

inline void printTermometer(byte sensorID)
{
  printTemperature(DS18B20_value[sensorID]);
}


