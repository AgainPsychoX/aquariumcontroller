#pragma once 

////////////////////////////////////////////////////////////////////////////////
// Drawing

/// Prints current hour 
inline void printTimeHour()
{
  lcd.write(('0' + PCF8583_controller.hour / 10));
  lcd.write(('0' + PCF8583_controller.hour % 10));
}

/// Prints current minute
inline void printTimeMinute()
{
  lcd.write(('0' + PCF8583_controller.minute / 10));
  lcd.write(('0' + PCF8583_controller.minute % 10));
}

/// Prints current second
inline void printTimeSecond()
{
  lcd.write(('0' + PCF8583_controller.second / 10));
  lcd.write(('0' + PCF8583_controller.second % 10));
}

/// Prints time in `hhmmss` format
inline void printTimeHHMMSS()
{
  printTimeHour();
  lcd.write(':');
  printTimeMinute();
  lcd.write(':');
  printTimeSecond();
}

/// Prints current day number
inline void printDateDay()
{
  lcd.write(('0' + PCF8583_controller.day / 10));
  lcd.write(('0' + PCF8583_controller.day % 10));
}

/// Prints current month 
inline void printDateMonth()
{
  lcd.print(MonthName[PCF8583_controller.month - 1]);
}

/// Prints current year
inline void printDateYear()
{
  lcd.print(PCF8583_controller.year);
}

/// Prints current day of week
inline void printDateDayOfWeek()
{
  lcd.print(DayName[PCF8583_controller.get_day_of_week()]);
  // @TODO , `get_day_of_week` aktualizuje sie dopiero po zapisaniu
}



////////////////////////////////////////////////////////////////////////////////
// Drawing (big characters)

#include "BigCharacters.hpp"

/// Draws current hour as big number
inline void drawBigDigitalTimeHour(byte hour, byte col, byte row = 0)
{
  drawBigNumber2Digit(PCF8583_controller.hour, col, row);
}
/// Draws current minute as big number
inline void drawBigDigitalTimeMinute(byte col, byte row = 0)
{
  drawBigNumber2Digit(PCF8583_controller.minute, col, row);
}
/// Draws current second as big number
inline void drawBigDigitalTimeSecond(byte col, byte row = 0)
{
  drawBigNumber2Digit(PCF8583_controller.second, col, row);
}


