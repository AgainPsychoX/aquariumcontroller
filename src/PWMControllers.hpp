#pragma once

////////////////////////////////////////////////////////////////////////////////
// Heading 

// Uses EEPROM
#include <EEPROM.h>

// Select address of (6 * number of PWM pins) free bytes of EEPROM to save light settings, comment if no saving
#define PWMSettingsEEPROMAddress 0

// Timed PWM controller class
struct TimedPWMController
{
  /* Fields */
  // Settings
  struct Settings {
    Time_HourMinute onTime  { 7, 30};
    Time_HourMinute offTime {21, 30};
    
    //Time_HourMinute risingUpTime = 15; // @TODO - hours not necessary
    byte risingUpTime = 15; // minutes
    byte maxPower = 255;  // 0-255
  } settings;
  
  // Config
  const byte pin : 7;
  
  // Current state
  bool state : 1;
  byte currentPower = 255;
  
  /* Operators */
  TimedPWMController (const byte pin)
    : pin(pin)
  {}
  
  /* Methods */
  void setState(byte power)
  {
    if (power == 0) {
      currentPower = 0;
      state = false;
      if (settings.risingUpTime == 0 && settings.maxPower == 255) {
        digitalWrite(pin, LOW);
      }
      else {
        analogWrite(pin, 0);
      }
    }
    else {
      currentPower = power;
      state = true;
      if (settings.risingUpTime == 0 && settings.maxPower == 255) {
        digitalWrite(pin, HIGH);
      }
      else {
        analogWrite(pin, currentPower); 
      }
    }
  }
  
  inline void update(const Time_hhmmss& currentTime)
  { 
    const unsigned long currentTimestamp = currentTime.toTimestamp();
    
    const unsigned long risingSeconds = settings.risingUpTime * 60;
    
    unsigned long lastOnTimestamp = settings.onTime.toTimestamp();
    if (currentTimestamp <= lastOnTimestamp) {
      lastOnTimestamp -= 24ul * 60ul * 60ul;
    }
    
    //unsigned long lastOffTimestamp = settings.offTime.toTimestamp();
    unsigned long lastOffTimestamp = settings.offTime.toTimestamp() - risingSeconds;
    if (currentTimestamp <= lastOffTimestamp) {
      lastOffTimestamp -= 24ul * 60ul * 60ul;
    }
    
    if (lastOnTimestamp > lastOffTimestamp) {
      // ON
      const unsigned long passedSeconds = currentTimestamp - lastOnTimestamp;
      
      if (passedSeconds < risingSeconds) {
        const float fraction = (float)passedSeconds / (float)risingSeconds;
        setState(static_cast<byte>(static_cast<int>(settings.maxPower * fraction)));
      }
      else {
        setState(settings.maxPower);
      }
    }
    else {
      // OFF
      const unsigned long passedSeconds = currentTimestamp - lastOffTimestamp;
      
      if (passedSeconds < risingSeconds) {
        const float fraction = 1 - ((float)passedSeconds / (float)risingSeconds);
        setState(static_cast<byte>(static_cast<int>(settings.maxPower * fraction)));
      }
      else {
        setState(0);
      }
    }
  }
  
  inline void readSettings(unsigned int address)
  {
    EEPROM.get(address, settings);
  }
  inline void saveSettings(unsigned int address)
  {
    EEPROM.put(address, settings);
  }
};

// Number of PWN pins
#define PWMPins_Length 3

// Structures constructed with PWN pin numbers.
TimedPWMController TimedPWNControllers[PWMPins_Length] = {{3}, {5}, {6}};



////////////////////////////////////////////////////////////////////////////////
// Functions

/// Reads light settings from EEPROM
void readPWMControllersSettings()
{
  for (unsigned char i = 0; i < PWMPins_Length; i++) {
    TimedPWNControllers[i].readSettings(PWMSettingsEEPROMAddress + sizeof(TimedPWMController::Settings) * i);
  }
}
/// Saves light settings to EEPROM
void savePWMControllersSettings()
{
  for (unsigned char i = 0; i < PWMPins_Length; i++) {
    TimedPWNControllers[i].saveSettings(PWMSettingsEEPROMAddress + sizeof(TimedPWMController::Settings) * i);
  }
}



////////////////////////////////////////////////////////////////////////////////
// Updating

inline void updatePWMControllers()
{
  Time_hhmmss currentTime {(byte)PCF8583_controller.hour, (byte)PCF8583_controller.minute, (byte)PCF8583_controller.second};
  
  for (unsigned char i = 0; i < PWMPins_Length; i++) {
    TimedPWNControllers[i].update(currentTime);
  }
}



////////////////////////////////////////////////////////////////////////////////
// Setup

inline void setupPWMControllers()
{
  Time_hhmmss currentTime {(byte)PCF8583_controller.hour, (byte)PCF8583_controller.minute, (byte)PCF8583_controller.second};
  
  for (unsigned char i = 0; i < PWMPins_Length; i++) {
    pinMode(TimedPWNControllers[i].pin, OUTPUT);
    TimedPWNControllers[i].readSettings(PWMSettingsEEPROMAddress + sizeof(TimedPWMController::Settings) * i);
    TimedPWNControllers[i].update(currentTime);
  }
}


