
* Rework `SoftwareSerial` using more modern C++ way 

	* [SoftwreSerial sources](https://github.com/codebendercc/arduino-library-files/blob/master/libraries/SoftwareSerial/SoftwareSerial.cpp)
	* [Simple C++11 compile-time map](https://stackoverflow.com/questions/16490835/how-to-build-a-compile-time-key-value-store) which could be used for delay lookups and etc.

* More interrupts (for inputs; featuring rendering etc; updating (temperature, time) by `loop` deamons ;) )

	* https://starter-kit.nettigo.pl/2018/04/przerwanie-na-kazdym-pinie-arduino-uno/
