
signed char timeFlow_hoursLeft = 0;

inline void updateSerialCommunication()
{
  Stream* stream = nullptr;
  if (Serial.available() > 0) {
    stream = static_cast<Stream*>(&Serial);
  }
  else if (BluetoothSerial.available() > 0) {
    stream = static_cast<Stream*>(&BluetoothSerial);
  }
  
  auto forceRead = [&stream](){
    while (stream->available() == 0); // @TODO , timeout (or use something from Stream.h
    if ((int)stream->peek() == 59) {
      Serial.println(59);
    }
    else {
      Serial.print((int)stream->peek());
      Serial.print(' ');
    }
    return stream->read();
  };
  
  if (stream != nullptr) {
    delay(17);
    switch (forceRead()) {
      case 'd':
        switch (forceRead()) {
          case '=': {
            // Set date command
            PCF8583_controller.year   = forceRead() + 2000;
            PCF8583_controller.month  = forceRead();
            PCF8583_controller.day    = forceRead();
            PCF8583_controller.set_time();
          } break;
          
          case '?': {
            // Get date command: d?
            stream->write('d');
            stream->write('=');
            stream->write(PCF8583_controller.year - 2000);
            stream->write(PCF8583_controller.month);
            stream->write(PCF8583_controller.day);
          } break;
          
          default:
            stream->write('?');
            break;
        }
        break;
        
      case 't':
        switch (forceRead()) {
          case '=': {
            // Set time command: t=hhmmss
            PCF8583_controller.hour   = forceRead();
            PCF8583_controller.minute = forceRead();
            PCF8583_controller.second = forceRead();
            PCF8583_controller.set_time();
          } break;
            
          case '?': {
            // Get time command: t?
            stream->write('t');
            stream->write('=');
            stream->write(PCF8583_controller.hour);
            stream->write(PCF8583_controller.minute);
            stream->write(PCF8583_controller.second);
          } break;
          
          default:
            stream->write('?');
            break;
        }
        break;
      
      case 'p': {
        unsigned char pwmID = forceRead();
        if (0 <= pwmID && pwmID < PWMPins_Length) {
          switch (forceRead()) {
            case '=': {
              // Set PWM controllers settings: pX=pppHHMMhhmmtt;
              // p1=2300715213030;p2=2551030173000;p3=1302200063030
              TimedPWNControllers[pwmID].settings.maxPower        = forceRead();
              TimedPWNControllers[pwmID].settings.onTime.hour     = forceRead();
              TimedPWNControllers[pwmID].settings.onTime.minute   = forceRead();
              TimedPWNControllers[pwmID].settings.offTime.hour    = forceRead();
              TimedPWNControllers[pwmID].settings.offTime.minute  = forceRead();
              TimedPWNControllers[pwmID].settings.risingUpTime    = forceRead();
              TimedPWNControllers[pwmID].update(Time_hhmmss{(byte)PCF8583_controller.hour, (byte)PCF8583_controller.minute, (byte)PCF8583_controller.second});
              TimedPWNControllers[pwmID].saveSettings(PWMSettingsEEPROMAddress + sizeof(TimedPWMController::Settings) * pwmID);
            } break;
              
            case '?': {
              // Get time command: pX?
              stream->write('p');
              stream->write(pwmID);
              stream->write('=');
              stream->write(TimedPWNControllers[pwmID].settings.maxPower);
              stream->write(TimedPWNControllers[pwmID].settings.onTime.hour);
              stream->write(TimedPWNControllers[pwmID].settings.onTime.minute);
              stream->write(TimedPWNControllers[pwmID].settings.offTime.hour);
              stream->write(TimedPWNControllers[pwmID].settings.offTime.minute);
              stream->write(TimedPWNControllers[pwmID].settings.risingUpTime);
            } break;
            
            case '%': {
              // Get current power: pX%
              stream->write('p');
              stream->write(pwmID);
              stream->write('%');
              stream->write(TimedPWNControllers[pwmID].currentPower);
            } break;
            
            default:
              stream->write('?');
              break;
          }
        }
      } break;
      
      // Time flow test
      case '-': {
        timeFlow_hoursLeft--;
        PCF8583_controller.hour = overflowMenuValue<0, 23>(PCF8583_controller.hour - 1);
        PCF8583_controller.set_time();
      } break;
      case '+': {
        timeFlow_hoursLeft++;
        PCF8583_controller.hour = overflowMenuValue<0, 23>(PCF8583_controller.hour + 1);
        PCF8583_controller.set_time();
      } break;
      case '=': {
        PCF8583_controller.hour = overflowMenuValue<0, 23>(PCF8583_controller.hour - timeFlow_hoursLeft);
        PCF8583_controller.set_time();
        timeFlow_hoursLeft = 0;
      } break;
      
      default:
        stream->write('?');
        break;
    }
    
    // Wait to end command
    byte wait;
    do {
      wait = forceRead(); // @TODO , timeout?
    } while (wait != ';');
    
    // End current output
    stream->write(';');
  }
}
