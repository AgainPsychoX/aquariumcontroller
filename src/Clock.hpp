#pragma once
/** @file Clock.hpp
 ** @description Manages system clock (PCF8583) and provide drawing function (for LiquidCrystal LCD, at least 20x2).
 **/

////////////////////////////////////////////////////////////////////////////////
// Heading

// Library for PCF8583T clock
#include <PCF8583.h>

// Clock controller
PCF8583 PCF8583_controller(0x50 << 1); // Adres I2C z skanera (<< 1 bo błąd? w bibliotece)

// Strings for days and months display names
const char* DayName[7] = {"Niedz", "Pon", "Wt", "Sr", "Czw", "Pt", "Sob"};
const char* MonthName[12] = {"sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paz", "lis", "gru"};



////////////////////////////////////////////////////////////////////////////////
// Setup

inline void setupClock() {
  ;
};



////////////////////////////////////////////////////////////////////////////////
// Update

inline void updateClock() {
  PCF8583_controller.get_time();
}


