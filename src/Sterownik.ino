/*
    Sterownik akwariowy 
    Zegar, termometr i oświetlenie ustawiane z menu (z duzymi literami)
		- z zegarem układzie PCF8583 i termometrami DS18B20
		  wyswietlane na LCD 4x20 HD44780
*/
////////////////////////////////////////////////////////////////////////////////
/* Biblioteki */
#include <Wire.h>
#include <LiquidCrystal_I2C.h> // LCD 4x20 HD44780

#define DEBUG 1

#include "DelayedConstruction.hpp"

/* Wyswietlacz (LCD 4x20 HD44780) */
//                    I2C,  EN, RW, RS, D4, D5, D6, D7, BL, BLPOL
LiquidCrystal_I2C lcd(0x27, 2,  1,  0,  4,  5,  6,  7,  3,  POSITIVE);
// Wymiary
#define ScreenMaxWidth   20
#define ScreenMaxHeight  4
// Duże znaki
#include "BigCharacters.hpp"

#include "Time.hpp"
#include "Clock.hpp"
#include "ClockDrawing.hpp"
#include "Termometer.hpp"
#include "TermometerDrawing.hpp"
#include "HeatingController.hpp"
#include "phSensor.hpp"
#include "PWMControllers.hpp"

// Virtual page
enum class ScreenPage {
  Clock,
  Status,
  Menu,
  TemperatureView
};
ScreenPage screenPage = ScreenPage::Clock;

// Menu 
#include "Menu.hpp"
#include "Menu_UseAction.hpp"
#include "Menu_GoAction.hpp"

#include "BluetoothQwertyServer.hpp"
allocateObject(BluetoothQwertyServer, bluetooth);

#include "Communication.hpp"



//////////////	//////////////////////////////////////////////////////////////////
/// Rysowanie (co klatke)
void draw()
{
  switch (screenPage) {
    // Ekran z zegarem (czas i data)
    case ScreenPage::Clock:
      #include "Pages/ClockScreenPage.hpp"
      break;
    
    // Ekran z stanen (czas, oświetlenie, temperatury, grzałka...)
    case ScreenPage::Status:
      #include "Pages/StatusScreenPage.hpp"
      break;
    
    // Ekran menu (generyczny)
    case ScreenPage::Menu:
      #include "Pages/MenuScreenPage.hpp"
      break;
      
    // Ekran z temperaturami
    case ScreenPage::TemperatureView:
      #include "Pages/TemperatureViewScreenPage.hpp"
      break;
  }
}



////////////////////////////////////////////////////////////////////////////////
/* Arduino - odświerzanie */
void loop(void)
{
  delay(1);
  
  bluetooth.update();
  
  /* Zegar */
  if (menuLevel != MenuLevel::TimeHours && menuLevel != MenuLevel::TimeMinutes) {
    if (menuLevel == MenuLevel::DateYears || menuLevel == MenuLevel::DateMonths || menuLevel == MenuLevel::DateDays) {
      unsigned short y = PCF8583_controller.year;
      unsigned short m = PCF8583_controller.month;
      unsigned short d = PCF8583_controller.day;
      updateClock();
      PCF8583_controller.year = y;
      PCF8583_controller.month = m;
      PCF8583_controller.day = d;
    }
    else {
      updateClock();
    }
  }

  updateTermometer();
  
  updateHeatingControllers();
  
  updatephSensorsSamplings();
  updatephSensorsValues();
  
  updatePWMControllers();
  
  updateMenu();
  
  ////////////////////////////////////////
  draw();
}



////////////////////////////////////////////////////////////////////////////////
/* Arduino - inicjalizacja */
void setup(void)
{
  Serial.begin(9600);
  
	constructObject(BluetoothQwertyServer, bluetooth,
		communicationHandler,
		7, // Recieving/Input pin
		8, // Sending/Output pin
		9600 // Baud rate/Speed
	);

  
  setupClock();
  
  /* Wyswietlacz */
  lcd.begin(ScreenMaxWidth, ScreenMaxHeight);
  lcd.clear();
  setupBigCharacters();
  
  setupMenu();
  
  setupTermometer();
  
  setupHeatingControllers();
  
  setupphSensors();
  
  setupPWMControllers();
}



