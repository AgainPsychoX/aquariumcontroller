
#ifdef DEBUG

inline byte SerialForceRead()
{
  while (Serial.available() == 0) {};
  return Serial.read();
}

signed char timeFlow_hoursLeft = 0;

inline void debugConsole()
{
  if (Serial.available() > 0) {
    switch (Serial.read()) {
      case 'd':
        // Set date command: d=YYMMDD;
        if (SerialForceRead() == '=') {
          PCF8583_controller.year   = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0')) + 2000;
          PCF8583_controller.month  = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          PCF8583_controller.day    = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          
          while (SerialForceRead() != ';') {};
          PCF8583_controller.set_time();
          Serial.println(F("date set!"));
        }
        break;
        
      case 't':
        // Set time command: t=hhmmss;
        if (SerialForceRead() == '=') {
          PCF8583_controller.hour   = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          PCF8583_controller.minute = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          PCF8583_controller.second = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          
          while (SerialForceRead() != ';') {};
          PCF8583_controller.set_time();
          Serial.println(F("time set!"));
        }
        break;
      
      // Set PWM controllers settings: pX=ttpppHHMMhhmm;
      // p1=3023007152100;p2=0025510301730;p3=3013022000630;
      case 'p': {
        signed char pwmID = (SerialForceRead() - 1 - '0');
        if (0 <= pwmID && pwmID < PWMPins_Length) {
          if (SerialForceRead() != '=') return;
          
          TimedPWNControllers[pwmID].settings.risingUpTime = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          TimedPWNControllers[pwmID].settings.maxPower = (byte) ((SerialForceRead() - '0') * 100 + (SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          TimedPWNControllers[pwmID].settings.onTime.hour    = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          TimedPWNControllers[pwmID].settings.onTime.minute  = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          TimedPWNControllers[pwmID].settings.offTime.hour   = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          TimedPWNControllers[pwmID].settings.offTime.minute = (byte) ((SerialForceRead() - '0') * 10 + (SerialForceRead() - '0'));
          TimedPWNControllers[pwmID].update(Time_hhmmss{(byte)PCF8583_controller.hour, (byte)PCF8583_controller.minute, (byte)PCF8583_controller.second});
          
          while (SerialForceRead() != ';') {};
          TimedPWNControllers[pwmID].saveSettings(PWMSettingsEEPROMAddress + sizeof(TimedPWMController::Settings) * pwmID);
          Serial.println(';');
        }
      } break;
      
      // Time flow test
      case '-': {
        timeFlow_hoursLeft--;
        PCF8583_controller.hour = overflowMenuValue<0, 23>(PCF8583_controller.hour - 1);
        PCF8583_controller.set_time();
      } break;
      case '+': {
        timeFlow_hoursLeft++;
        PCF8583_controller.hour = overflowMenuValue<0, 23>(PCF8583_controller.hour + 1);
        PCF8583_controller.set_time();
      } break;
      case '=': {
        PCF8583_controller.hour = overflowMenuValue<0, 23>(PCF8583_controller.hour - timeFlow_hoursLeft);
        PCF8583_controller.set_time();
        timeFlow_hoursLeft = 0;
      } break;
      
      // Test
      case '?':
        Serial.println(" ");
        
      default:
        break;
    }
    /**/
  }
}

#else

inline void debugConsole()
{
  ;
}

#endif
