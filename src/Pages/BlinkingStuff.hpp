
{
  switch (menuLevel) {
    case MenuLevel::PWMControllerRisingUp:
      // Blink rising up time
      if (millis() % 1000 > 500) {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_RisingUp_Index][PWMControllerMenu_RisingUp_Offset + 0] = ' ';
        MenuStrings_PWMControllerMenu[PWMControllerMenu_RisingUp_Index][PWMControllerMenu_RisingUp_Offset + 1] = ' ';
      }
      else {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_RisingUp_Index][PWMControllerMenu_RisingUp_Offset + 0] = 
          (TimedPWNControllers[menuSelectionPWM].settings.risingUpTime > 9 
            ? (char) (TimedPWNControllers[menuSelectionPWM].settings.risingUpTime / 10 + '0') 
            : ' ');
        MenuStrings_PWMControllerMenu[PWMControllerMenu_RisingUp_Index][PWMControllerMenu_RisingUp_Offset + 1] = (char) (TimedPWNControllers[menuSelectionPWM].settings.risingUpTime % 10 + '0');
      }
      break;
    
    case MenuLevel::PWMControllerMaxPower:
      // Blink max PWM power
      if (millis() % 1000 > 500) {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 0] = ' ';
        MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 1] = ' ';
        MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 2] = ' ';
      }
      else {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 0] = (TimedPWNControllers[menuSelectionPWM].settings.maxPower == 255 ? '1' : ' ');
        MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 1] = (menuMaxPowerPercent > 9 ? (char) (menuMaxPowerPercent / 10 % 10 + '0') : ' ');
        MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 2] = (char) (menuMaxPowerPercent % 10 + '0');
      }
      break;
    
    case MenuLevel::PWMControllerOnTimeHours:
      // Blink on time (hours)
      if (millis() % 1000 > 500) {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 0] = ' ';
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 1] = ' ';
      }
      else {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 0] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.hour / 10 + '0');
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 1] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.hour % 10 + '0');
      }
      break;
    
    case MenuLevel::PWMControllerOnTimeMinutes:
      // Blink on time (minute)
      if (millis() % 1000 > 500) {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 0 + 3] = ' ';
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 1 + 3] = ' ';
      }
      else {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 0 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.minute / 10 + '0');
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 1 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.minute % 10 + '0');
      }
      break;
    
    case MenuLevel::PWMControllerOffTimeHours:
      // Blink off time (hour)
      if (millis() % 1000 > 500) {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 0] = ' ';
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 1] = ' ';
      }
      else {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 0] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.hour / 10 + '0');
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 1] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.hour % 10 + '0');
      }
      break;
    
    case MenuLevel::PWMControllerOffTimeMinutes:
      // Blink off time (minute) 
      if (millis() % 1000 > 500) {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 0 + 3] = ' ';
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 1 + 3] = ' ';
      }
      else {
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 0 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.minute / 10 + '0');
        MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 1 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.minute % 10 + '0');
      }
      break;
    
    case MenuLevel::HeatingSettings_G1_OnTemperature:
      // Blink G1 on temperature
      if (millis() % 1000 > 500) {
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OnTemperature_Index][MenuStrings_HeatingSettings_G1_OnTemperature_Offset + 0] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OnTemperature_Index][MenuStrings_HeatingSettings_G1_OnTemperature_Offset + 1] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OnTemperature_Index][MenuStrings_HeatingSettings_G1_OnTemperature_Offset + 3] = ' ';
      }
      else {
        const String g1_onT(HeatingControllers[0].settings.minTemperature, 1);
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OnTemperature_Index][MenuStrings_HeatingSettings_G1_OnTemperature_Offset + 0] = g1_onT[0];
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OnTemperature_Index][MenuStrings_HeatingSettings_G1_OnTemperature_Offset + 1] = g1_onT[1];
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OnTemperature_Index][MenuStrings_HeatingSettings_G1_OnTemperature_Offset + 3] = g1_onT[3];
      }
      break;
      
    case MenuLevel::HeatingSettings_G1_OffTemperature:
      // Blink G1 off temperature
      if (millis() % 1000 > 500) {
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OffTemperature_Index][MenuStrings_HeatingSettings_G1_OffTemperature_Offset + 0] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OffTemperature_Index][MenuStrings_HeatingSettings_G1_OffTemperature_Offset + 1] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OffTemperature_Index][MenuStrings_HeatingSettings_G1_OffTemperature_Offset + 3] = ' ';
      }
      else {
        const String g1_offT(HeatingControllers[0].settings.maxTemperature, 1);
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OffTemperature_Index][MenuStrings_HeatingSettings_G1_OffTemperature_Offset + 0] = g1_offT[0];
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OffTemperature_Index][MenuStrings_HeatingSettings_G1_OffTemperature_Offset + 1] = g1_offT[1];
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OffTemperature_Index][MenuStrings_HeatingSettings_G1_OffTemperature_Offset + 3] = g1_offT[3];
      }
      break;
    
    case MenuLevel::HeatingSettings_G1_Alarm:
      // Blink G1 alarm mode
      if (millis() % 1000 > 500) {
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 0] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 1] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 2] = ' ';
      }
      else {
        if (temperatureAlarms[0].settings.active) {
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 0] = 't';
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 1] = 'a';
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 2] = 'k';
        }
        else {
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 0] = 'n';
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 1] = 'i';
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 2] = 'e';
        }
      }
      break;
    
    case MenuLevel::HeatingSettings_G2_OnTemperature:
      // Blink G2 on temperature
      if (millis() % 1000 > 500) {
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OnTemperature_Index][MenuStrings_HeatingSettings_G2_OnTemperature_Offset + 0] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OnTemperature_Index][MenuStrings_HeatingSettings_G2_OnTemperature_Offset + 1] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OnTemperature_Index][MenuStrings_HeatingSettings_G2_OnTemperature_Offset + 3] = ' ';
      }
      else {
        const String G2_onT(HeatingControllers[1].settings.minTemperature, 1);
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OnTemperature_Index][MenuStrings_HeatingSettings_G2_OnTemperature_Offset + 0] = G2_onT[0];
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OnTemperature_Index][MenuStrings_HeatingSettings_G2_OnTemperature_Offset + 1] = G2_onT[1];
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OnTemperature_Index][MenuStrings_HeatingSettings_G2_OnTemperature_Offset + 3] = G2_onT[3];
      }
      break;
      
    case MenuLevel::HeatingSettings_G2_OffTemperature:
      // Blink G2 off temperature
      if (millis() % 1000 > 500) {
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OffTemperature_Index][MenuStrings_HeatingSettings_G2_OffTemperature_Offset + 0] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OffTemperature_Index][MenuStrings_HeatingSettings_G2_OffTemperature_Offset + 1] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OffTemperature_Index][MenuStrings_HeatingSettings_G2_OffTemperature_Offset + 3] = ' ';
      }
      else {
        const String G2_offT(HeatingControllers[1].settings.maxTemperature, 1);
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OffTemperature_Index][MenuStrings_HeatingSettings_G2_OffTemperature_Offset + 0] = G2_offT[0];
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OffTemperature_Index][MenuStrings_HeatingSettings_G2_OffTemperature_Offset + 1] = G2_offT[1];
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OffTemperature_Index][MenuStrings_HeatingSettings_G2_OffTemperature_Offset + 3] = G2_offT[3];
      }
      break;
    
    case MenuLevel::HeatingSettings_G2_Alarm:
      // Blink G2 alarm mode
      if (millis() % 1000 > 500) {
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 0] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 1] = ' ';
        MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 2] = ' ';
      }
      else {
        if (temperatureAlarms[1].settings.active) {
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 0] = 't';
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 1] = 'a';
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 2] = 'k';
        }
        else {
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 0] = 'n';
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 1] = 'i';
          MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 2] = 'e';
        }
      }
      break;
    
    default:
      // Nothing
      break;
  }
}