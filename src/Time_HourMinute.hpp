#pragma once

#if __cplusplus < 201402L
#define constexpr
#define static_assert(...) /**/
#endif

/// Holds time as hour/minute
struct Time_HourMinute
{
	/* Fields */
	uint8_t hour;
	uint8_t minute;
	
	/* Operators */
	constexpr Time_HourMinute(uint8_t hour, uint8_t minute) noexcept
		: hour(hour), minute(minute)
	{}
	
	friend constexpr bool operator ==	(const Time_HourMinute& lhs, 	const Time_HourMinute& rhs) noexcept;
	friend constexpr bool operator !=	(const Time_HourMinute& lhs, 	const Time_HourMinute& rhs) noexcept;
	friend constexpr bool operator >	(const Time_HourMinute& lhs, 	const Time_HourMinute& rhs) noexcept;
	friend constexpr bool operator <	(const Time_HourMinute& lhs, 	const Time_HourMinute& rhs) noexcept;
	friend constexpr bool operator >=	(const Time_HourMinute& lhs,	const Time_HourMinute& rhs) noexcept;
	friend constexpr bool operator <=	(const Time_HourMinute& lhs, 	const Time_HourMinute& rhs) noexcept;
  
  /* Methods */
  // template < unit > ;_;
  unsigned long toTimestamp() const 
  {
    return 
    // Seconds
    (
      // Minutes
      (
        // Hours
        (
          7ul * 24ul + // 7 days as bloat time ._.
          static_cast<unsigned long>(hour)
        )
        * 60ul + 
        static_cast<unsigned long>(minute)
      )
      * 60ul
    );
  }
};

constexpr bool operator == (const Time_HourMinute& lhs, const Time_HourMinute& rhs) noexcept
{
	if (lhs.hour 	!= rhs.hour) 	return false;
	if (lhs.minute 	!= rhs.minute) 	return false;
	return true;
}
static_assert(Time_HourMinute{9, 30} == Time_HourMinute{9, 30}, "");

constexpr bool operator != (const Time_HourMinute& lhs, const Time_HourMinute& rhs) noexcept
{
	return !(lhs == rhs);
}
static_assert(Time_HourMinute{9, 30} != Time_HourMinute{9, 45}, "");

constexpr bool operator > (const Time_HourMinute& lhs, const Time_HourMinute& rhs) noexcept
{
	if (lhs.hour > rhs.hour) return true;
	if (lhs.hour < rhs.hour) return false;
	return lhs.minute > rhs.minute;
}
static_assert(Time_HourMinute{10, 30} > Time_HourMinute{9, 45}, "");

constexpr bool operator < (const Time_HourMinute& lhs, const Time_HourMinute& rhs) noexcept
{
	if (lhs.hour < rhs.hour) return true;
	if (lhs.hour > rhs.hour) return false;
	return lhs.minute < rhs.minute;
}
static_assert(Time_HourMinute{9, 30} < Time_HourMinute{9, 45}, "");

constexpr bool operator >= (const Time_HourMinute& lhs, const Time_HourMinute& rhs) noexcept
{
	if (lhs.hour > rhs.hour) return true;
	if (lhs.hour < rhs.hour) return false;
	return lhs.minute >= rhs.hour;
}
static_assert(Time_HourMinute{9, 45} >= Time_HourMinute{8, 30}, "");

constexpr bool operator <= (const Time_HourMinute& lhs, const Time_HourMinute& rhs) noexcept
{
	if (lhs.hour < rhs.hour) return true;
	if (lhs.hour > rhs.hour) return false;
  return lhs.minute <= rhs.minute;
}
static_assert(Time_HourMinute{7, 45} <= Time_HourMinute{9, 45}, "");

#if __cplusplus < 201402L
#undef constexpr
#undef static_assert
#endif
